class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :package_id
      t.integer :plan_id
      t.integer :no_of_items
      t.decimal :total_amount

      t.timestamps
    end
  end
end
