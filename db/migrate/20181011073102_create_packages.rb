class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string :name
      t.text :description
      t.boolean :is_active,:defalt=> false;
      t.integer :service_id
      t.integer :city_id
      t.integer :division_id

      t.timestamps
    end
  end
end
