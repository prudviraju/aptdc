class RemoveImageIdFromCities < ActiveRecord::Migration[5.2]
  def change
    remove_column :cities, :image_id, :integer
    remove_column :divisions, :image_id, :integer
    remove_column :packages, :image_id, :integer
    remove_column :plans, :image_id, :integer
    remove_column :services, :image_id, :integer
  end
end
