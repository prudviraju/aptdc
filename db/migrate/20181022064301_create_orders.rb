class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :merchant_transaction_id
      t.decimal :transaction_amount
      t.string :buyer_phone_no
      t.string :buyer_email_address
      t.string :status
      t.string :first_name
      t.string :last_name
      t.string :address1
      t.string :address2
      t.string :town
      t.string :state
      t.string :postal_code
      t.string :transaction_details

      t.timestamps
    end
  end
end
