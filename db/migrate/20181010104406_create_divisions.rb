class CreateDivisions < ActiveRecord::Migration[5.2]
  def change
    create_table :divisions do |t|
      t.string :name
      t.text :content
      t.integer :city_id

      t.timestamps
    end
  end
end
