class AddImageIdToCities < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :image_id, :integer
    add_column :divisions, :image_id, :integer
    add_column :packages, :image_id, :integer
    add_column :plans, :image_id, :integer
    add_column :services, :image_id, :integer
  end
end
