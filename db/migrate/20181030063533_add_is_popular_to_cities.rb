class AddIsPopularToCities < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :is_popular, :boolean,:defalt=> false;
    add_column :divisions, :is_popular, :boolean,:defalt=> false;
  end
end
