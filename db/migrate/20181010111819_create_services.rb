class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :name
      t.text :description
      t.integer :division_id
      t.integer :city_id
      t.boolean :is_active,:defalt => false ;

      t.timestamps
    end
  end
end
