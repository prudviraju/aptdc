class RemoveCityIdFromServices < ActiveRecord::Migration[5.2]
  def change
    remove_column :services, :city_id, :integer
    remove_column :services, :division_id, :integer
  end
end
