class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items do |t|
      t.integer :package_id
      t.integer :plan_id
      t.integer :no_of_items
      t.string :session_id
      t.decimal :price

      t.timestamps
    end
  end
end
