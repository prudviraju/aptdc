class CreatePlans < ActiveRecord::Migration[5.2]
  def change
    create_table :plans do |t|
      t.integer :package_id
      t.string :pakage_type
      t.string :name
      t.text :description
      t.decimal :price
      t.boolean :is_active,:defalt=> false;

      t.timestamps
    end
  end
end
