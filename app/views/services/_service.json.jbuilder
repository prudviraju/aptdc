json.extract! service, :id, :name, :description, :division_id, :city_id, :is_active, :created_at, :updated_at
json.url service_url(service, format: :json)
