class CitiesController < ApplicationController
  include ApplicationHelper
  include CitiesHelper
  include CcAvenueHelper
  before_action :set_city, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
  skip_authorize_resource :only => [:index,:homePage, :location_based_packages, :destinations, :aboutUs, :contactUs, :service_based_city_pakages, :add_services_to_session, :service_based_packages, :service_based_city_pakages, :service_based_packages1, :billing_detail, :change_cart_items, :place_order, :service_based_city_pakages1]
  # GET /cities
  # GET /cities.json
  def index
    @cities = City.all
  end

  # GET /cities/1
  # GET /cities/1.json
  def show
  end

  # GET /cities/new
  def new
    @city = City.new
  end

  # GET /cities/1/edit
  def edit
  end

  # POST /cities
  # POST /cities.json
  def create
    @city = City.new(city_params)
    respond_to do |format|
      if @city.save

        format.html {redirect_to "/cities", notice: 'City was successfully created.'}
        format.json {render :show, status: :created, location: @city}
      else
        format.html {render :new}
        format.json {render json: @city.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /cities/1
  # PATCH/PUT /cities/1.json
  def update
    respond_to do |format|
      if @city.update(city_params)
        format.html {redirect_to "/cities", notice: 'City was successfully updated.'}
        format.json {render :show, status: :ok, location: @city}
      else
        format.html {render :edit}
        format.json {render json: @city.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /cities/1
  # DELETE /cities/1.json
  def destroy
    @city.destroy
    respond_to do |format|
      format.html {redirect_to cities_url, notice: 'City was successfully destroyed.'}
      format.json {head :no_content}
    end
  end


  def homePage
    session[SessionConstants::SELECTED_MENU_ITEM] = 'homePage'
    @services = Service.all
    @cities = City.where("is_popular=?", true)
    @locations = Division.where("is_popular=?", true)
    render :layout => 'application1'
  end

  def location_based_packages
    @division = Division.find_by_id(params[:location_id]) if params[:location_id].present?
    @packages = Package.where('division_id=?', @division.id) if @division.present?
    render :layout => 'application1'
  end

  def destinations
    session[SessionConstants::SELECTED_MENU_ITEM] = 'destinations'
    @services = Service.all
    @cities = City.all
    render :layout => 'application1'
  end

  def aboutUs
    session[SessionConstants::SELECTED_MENU_ITEM] = 'aboutUs'
    render :layout => 'application1'
  end

  def contactUs
    session[SessionConstants::SELECTED_MENU_ITEM] = 'contactUs'
    render :layout => 'application1'
  end

  def add_services_to_session
    @services = Service.all
    session[SessionConstants::SESSION_SERVICES] = ''
    session[SessionConstants::SESSION_SERVICES] = params[:service]
    if session[SessionConstants::SESSION_SERVICES].present? && !session[SessionConstants::SESSION_SERVICES].to_i.eql?(0)
      @cities = []
      @service_base_cities = []
      @packages = Package.where("service_id=?", session[SessionConstants::SESSION_SERVICES])
      @packages.each do |package|
        @service_base_cities << package.city_id if !@service_base_cities.include?(package.city_id)
      end
      @service_base_cities.each do |sbc|
        @city = City.find_by_id(sbc)
        @cities << @city
      end
    else
      @cities = City.all
    end
  end

  def service_based_city_pakages
    @location_based_packages = []
    if params[:city_id].present?
      @city = City.find_by_id(params[:city_id])
      @divisions = Division.where("city_id =?", @city.id) if @city.present?
      @service = Service.find_by_id(params[:service_id]) if params[:service_id].present?
      @divisions.each do |division|
        if @service.present?
          @packages = Package.where("city_id =? and service_id =? and division_id =?", @city.id, @service.id, division.id)
        else
          @packages = Package.where("city_id =? and division_id=?", @city.id, division.id)
        end
        @location_based_packages << LocationBasedPackages.new(division.name, @packages) if @packages.present?
      end
    end
    render :layout => 'application1'
  end


  def service_based_city_pakages1

    if params[:city_id].present?
      @city = City.find_by_id(params[:city_id])
      @service = Service.find_by_id(params[:service_id]) if params[:service_id].present?
      if @service.present?
        @packages = Package.where("city_id =? and service_id =?", @city.id, @service.id).order("division_id desc")
      else
        @packages = Package.where("city_id =?", @city.id).order("division_id desc")
      end
    end
    render :layout => 'application1'
  end


  def service_based_packages

    @service_base_cities = []
    @cities = []

    @service = Service.find_by_id(params[:service_id]) if params[:service_id].present?
    @allpackages = Package.where("service_id=?", @service.id) if @service.present?
    if @allpackages.present?
      @allpackages.each do |package|
        @service_base_cities << package.city_id if !@service_base_cities.include?(package.city_id)
      end
      @service_base_cities.each do |sbc|
        @city = City.find_by_id(sbc)
        @cities << @city
      end
    end

    render :layout => 'application1'
  end

  def service_based_packages1

    @service_base_cities = []
    @cities = []

    @service = Service.find_by_id(params[:service_id]) if params[:service_id].present?
    @allpackages = Package.where("service_id=?", @service.id) if @service.present?
    if @allpackages.present?
      @allpackages.each do |package|
        @service_base_cities << package.city_id if !@service_base_cities.include?(package.city_id)
      end
      @service_base_cities.each do |sbc|
        @city = City.find_by_id(sbc)
        @cities << @city
      end
    end

    render :layout => 'application1'
  end

  def billing_detail
    @package = Package.find_by_id(params[:package_id])
    @order = Order.new
    render :layout => 'application2'
  end

  def change_cart_items
    session[SessionConstants::SESSION_ID] = UUIDTools::UUID.random_create.to_s.delete '-' if session[SessionConstants::SESSION_ID].blank?
    @plan = Plan.find_by_package_id_and_pakage_type(params[:package_id], params[:package_type])
    if @plan.present?
      @cartItem = CartItem.find_by_plan_id_and_session_id(@plan.id, session[SessionConstants::SESSION_ID])
      if @cartItem.blank? && !params[:no_of_items].equal?(0)
        @cartItem = CartItem.new
        @cartItem.package_id = @plan.package_id
        @cartItem.plan_id = @plan.id
        @cartItem.no_of_items = params[:no_of_items]
        @cartItem.session_id = session[SessionConstants::SESSION_ID]
        @cartItem.price = (@plan.price.to_d * params[:no_of_items].to_i).to_d
        @cartItem.save
      else
        if params[:no_of_items].to_i == 0
          @cartItem.destroy
        else
          @cartItem.no_of_items = params[:no_of_items]
          @cartItem.price = (@plan.price.to_d * params[:no_of_items].to_i).to_d
          @cartItem.save
        end
      end
    end
  end

  def place_order
    @order = Order.new
    @order.first_name = params[:order][:first_name]
    @order.last_name = params[:order][:last_name]
    @order.buyer_phone_no = params[:order][:buyer_phone_no]
    @order.buyer_email_address = params[:order][:buyer_email_address]
    @order.status = 'Created'
    random_id = UUIDTools::UUID.random_create.to_s.delete '-'
    random_id[0..2] = ''
    @order.merchant_transaction_id = random_id
    @order.save!
    @cartItems = CartItem.where('session_id =?', session[SessionConstants::SESSION_ID])
    totalPrice = 0.0
    @cartItems.each do |item|
      @order_item = OrderItem.new
      @order_item.order_id = @order.id
      @order_item.package_id = item.package_id
      @order_item.plan_id = item.plan_id
      @order_item.total_amount = item.price
      @order_item.no_of_items = item.no_of_items
      totalPrice += item.price
      @order_item.save
    end
    @order.transaction_amount = totalPrice
    @order.save
    if @order.present? && @order_item.present? && params[:commit].present?
      @all_order_items = OrderItem.where("order_id=?", @order.id)
      sms = "Dear #{@order.first_name}Your order was booked at APTDC with OrderID #{@order.id},TrnID #{@order.merchant_transaction_id},Fare #{@order.transaction_amount}."
      send_sms(@order.buyer_phone_no, sms, 'order booked', @order.id)
      UserMailer.orderbooked(@order, @all_order_items).deliver
      reset_session
      payment_url = cc_avenue_details(@order, 'http://java.meritcampus.com/pay_zippy_response')
      redirect_to payment_url
    else
      redirect_to "/cities/billing_detail?package_id=#{params[:package_id]}"
    end
  end


  def pay_zippy_response

  end


  private

  # Use callbacks to share common setup or constraints between actions.
  def set_city
    @city = City.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def city_params
    params.require(:city).permit(:name, :description, :estimated_visit_days, :image, :is_popular)
  end
end
