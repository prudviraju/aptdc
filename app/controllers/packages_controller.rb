class PackagesController < ApplicationController
  before_action :set_package, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /packages
  # GET /packages.json
  def index
    @packages = Package.all
  end

  # GET /packages/1
  # GET /packages/1.json
  def show
  end

  # GET /packages/new
  def new
    @package = Package.new
  end

  # GET /packages/1/edit
  def edit
    @adult_price = Plan.find_by_package_id_and_pakage_type(@package.id,'Adults')
    @child_price = Plan.find_by_package_id_and_pakage_type(@package.id,'Child')
  end

  # POST /packages
  # POST /packages.json
  def create
    @package = Package.new(package_params)

    respond_to do |format|
      if @package.save
        if params[:adult_price].present?
          @plan = Plan.new
          @plan.package_id = @package.id
          @plan.pakage_type = 'Adults'
          @plan.name = 'Adults'
          @plan.price = params[:adult_price].to_d
          @plan.is_active = true
          @plan.save
        end
        if params[:child_price].present?
          @plan = Plan.new
          @plan.package_id = @package.id
          @plan.pakage_type = 'Child'
          @plan.name = 'Child'
          @plan.price = params[:child_price].to_d
          @plan.is_active = true
          @plan.save
        end
        format.html { redirect_to "/packages", notice: 'Package was successfully created.' }
        format.json { render :show, status: :created, location: @package }
      else
        format.html { render :new }
        format.json { render json: @package.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /packages/1
  # PATCH/PUT /packages/1.json
  def update
    respond_to do |format|
      if @package.update(package_params)

        if params[:adult_price].present?
          @plan = Plan.find_by_package_id_and_pakage_type(@package.id,'Adults')
          @plan.price = params[:adult_price]
          @plan.save
        end
        if params[:child_price].present?
          @plan = Plan.find_by_package_id_and_pakage_type(@package.id,'Child')
          @plan.price = params[:child_price]
          @plan.save
        end

        format.html { redirect_to "/packages", notice: 'Package was successfully updated.' }
        format.json { render :show, status: :ok, location: @package }
      else
        format.html { render :edit }
        format.json { render json: @package.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /packages/1
  # DELETE /packages/1.json
  def destroy
    @package.destroy
    respond_to do |format|
      format.html { redirect_to packages_url, notice: 'Package was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package
      @package = Package.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def package_params
      params.require(:package).permit(:name, :description, :is_active, :service_id, :city_id, :division_id,:image)
    end
end
