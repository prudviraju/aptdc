class SessionsController < ApplicationController
  def new
    render :layout => nil
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to "/cities", notice: "Logged in!"
    else
      flash.now[:alert] = "Email or password is invalid"
      render :action => 'new', :layout => nil
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to "/login", notice: "Logged out!"
  end
end