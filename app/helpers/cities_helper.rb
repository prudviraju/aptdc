module CitiesHelper
  class LocationBasedPackages
    attr_accessor :division_name, :packages
    def initialize(division_name, packages)
      @division_name = division_name
      @packages = packages
    end
  end

end
