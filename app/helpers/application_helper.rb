module ApplicationHelper


  #TEST
  MERCHANT_ID = "test_t850"
  SECRET_KEY_TO_GENERATE_HASH = "1f84b4dd74366eb672750f7dcee7a1e9c6e526eaddf6b48001294638374a2333"
  #PRODUCTION
  #MERCHANT_ID = "JAVAMERITCAMPUS"
  #SECRET_KEY_TO_GENERATE_HASH = "a9d16501d8e4d51b847fe4f181aa06b6426fc7c1e21617a78a63c5894b9a7597"

  MERCHANT_KEY_ID = "payment"


  module SessionConstants
    SESSION_ID = :session_id
    SELECTED_MENU_ITEM =:selected_menu_item
    SESSION_SERVICES = :session_services
  end


  def get_index_text(value)
    "<div class=\"row\">
    <div class=\"col-sm-12\" style=\"padding-left: 0px;\">
    <div class=\"head_style\">" "#{value}</div></div></div>".html_safe
  end

  def self.upload_image(params_image)

    puts "#####################################"
    puts "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
    puts params_image
    puts "#####################################"


    image = Image.new
    image.image_path = params_image
    image.save
    path = Rails.root.to_s + "/public" + image.image_path.url(:original).split('?')[0]
    url = "cwebp -q 80 #{path} -o #{path.gsub('.jpg', '.webp').gsub('.png', '.webp')}"
    puts url
    system(url)
    image
  end

  def save_image(decode_image)
    image = Image.new
    image.image_path = process_photo(decode_image)
    image.save
    url = "cwebp -q 80 #{Rails.root.to_s + "/public/assets/image/#{image.id}/original/picture.png"} -o #{Rails.root.to_s + "/public/assets/image/#{image.id}/original/picture.webp"}"
    system(url)
    image
  end

  def process_photo(photo)
    data = nil
    if !photo.blank?
      data = StringIO.new(Base64.decode64(photo))
      data.class.class_eval {attr_accessor :original_filename, :content_type}
      data.original_filename = 'picture.png'
      data.content_type = 'image/png'
    end
    data
  end

  def self.get_root_url
    "http://rybaz.cerone-software.com"
  end


  def cc_avenue_details(payment_details, return_url)
    values = {
        :order_id => payment_details.merchant_transaction_id,
        :amount => payment_details.transaction_amount,
        :currency => 'INR',
        :merchant_id => '44901',
        :redirect_url => return_url,
        :cancel_url => return_url,
        :language => 'EN',
        :billing_name => payment_details.first_name + (payment_details.last_name.present? ? ' ' + payment_details.last_name : ''),
        :billing_address => payment_details.address1,
        :billing_city => payment_details.town,
        :billing_zip => payment_details.postal_code,
        :billing_tel => payment_details.buyer_phone_no,
        :billing_email => payment_details.buyer_email_address,
        :billing_state => payment_details.state,
        :billing_country => 'India'
    }
    #"http://localhost:3000/cc_avenue/ccavRequestHandler?" + values.to_query
    "http://java.meritcampus.com/cc_avenue/ccavRequestHandler?" + values.to_query
  end


  def send_sms(mobile_number, original, message_type, order_id)

    puts ":########################"
    message = original.dup
    puts message
    puts ":########################"

    if mobile_number.to_s.strip.length == 10
      message.gsub!('%', "%25")
      message.gsub!('&', "%26")
      message.gsub!('+', "%2B")
      message.gsub!('#', "%23")
      message.gsub!('=', "%3D")
      message.gsub!(' ', "%20")
      url = "http://sms.txtdesk.com/WebServiceSMS.aspx?User=APTDC&passwd=APTDC123&mobilenumber=91"+mobile_number+"&sid=APTDCT&message="+message+"&fl=0&gwid=2"
      %x[echo "#{url}" >> sms-sending.log]
      %x[curl "#{url}" >> sms-sending.log]
      %x[echo "\n\n--------------------\n\n" >> sms-sending.log]

      # smslog = SmsLog.new
      # smslog.mobile_number = mobile_number
      # smslog.message = original
      # smslog.message_type = message_type
      # smslog.user_id = order_id
      # smslog.save
      # if user_id != -1
      #   userlog = UserLog.new
      #   userlog.mobile_number = mobile_number
      #   userlog.user_id = order_id
      #   userlog.action = "Sms Sent"
      #   userlog.observation = original
      #   userlog.actor = "None"
      #   userlog.save
      # end
    end
  end

end
