class UserMailer < ActionMailer::Base
  default from:"\"APTDC\" <aptdcupdates@gmail.com>"
  default_url_options[:host] = "java.meritcampus.com"

  def orderbooked(order,orderItems)
    @order = order
    @orderItems = orderItems
    mail(:to => @order.buyer_email_address.to_s, :subject => "Tickets Booked", :Bcc => 'saisrujan0929@gmail.com')
  end
end


