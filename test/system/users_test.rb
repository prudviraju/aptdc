require "application_system_test_case"

class UsersTest < ApplicationSystemTestCase
  setup do
    @user = users(:one)
  end

  test "visiting the index" do
    visit users_url
    assert_selector "h1", text: "Users"
  end

  test "creating a User" do
    visit users_url
    click_on "New User"

    fill_in "Crypted Password", with: @user.crypted_password
    fill_in "Email", with: @user.email
    fill_in "Is Admin", with: @user.is_admin
    fill_in "Is Citiuser", with: @user.is_citiUser
    fill_in "Is Division User", with: @user.is_division_user
    fill_in "Is Superadmin", with: @user.is_superadmin
    fill_in "Name", with: @user.name
    fill_in "Password", with: @user.password
    fill_in "Password Conformation", with: @user.password_conformation
    fill_in "Password Salt", with: @user.password_salt
    fill_in "Perishable Token", with: @user.perishable_token
    fill_in "Persistence Token", with: @user.persistence_token
    fill_in "Phone Number", with: @user.phone_number
    click_on "Create User"

    assert_text "User was successfully created"
    click_on "Back"
  end

  test "updating a User" do
    visit users_url
    click_on "Edit", match: :first

    fill_in "Crypted Password", with: @user.crypted_password
    fill_in "Email", with: @user.email
    fill_in "Is Admin", with: @user.is_admin
    fill_in "Is Citiuser", with: @user.is_citiUser
    fill_in "Is Division User", with: @user.is_division_user
    fill_in "Is Superadmin", with: @user.is_superadmin
    fill_in "Name", with: @user.name
    fill_in "Password", with: @user.password
    fill_in "Password Conformation", with: @user.password_conformation
    fill_in "Password Salt", with: @user.password_salt
    fill_in "Perishable Token", with: @user.perishable_token
    fill_in "Persistence Token", with: @user.persistence_token
    fill_in "Phone Number", with: @user.phone_number
    click_on "Update User"

    assert_text "User was successfully updated"
    click_on "Back"
  end

  test "destroying a User" do
    visit users_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User was successfully destroyed"
  end
end
