$(document).ready(function() {
	let toShow = [];
	let noEl = [];

	$('.top-right1 a').click(function(e) {

		$(e.target).toggleClass('tag-active1');
		let getId = e.target.id;
		let tagName = e.target.innerHTML;

		if (toShow.includes(getId)) {

			for (var i = 0; i < toShow.length; i ++) {
			    if (toShow[i] == getId) { 
			        toShow.splice(i, 1);
			        break;
			    }
			}
		}

		else {
			toShow.push(getId);
		}

		if	(noEl.includes(getId))  {
			for (let j = 0; j < noEl.length; j ++) {
			    if (noEl[j] == getId) { 
			    	$(`.list-body1 .${getId}`).fadeOut('fast');
			        noEl.splice(j, 1);
			        break;
			    }
			}
		
		}

		else if ( $('.top-right1 a'+'.'+getId).hasClass('tag-active1') && ($('.list-el1'+'.'+getId).length === 0) ) {
	   		 	$('.list-body1').append(`<p class="tag-no-result ${getId}">No results for: ${tagName}</p>`);
	   		 	noEl.push(getId);
		}

		if (toShow.length === 0) {
				$('#main1 .list-el1').fadeIn('fast');
				$(`.list-body1 .tag-no-result1`).fadeOut('fast');
			}

		else {
				$('#main1 .list-el1').fadeOut('fast');
		}

		$.each(toShow, function(i, b) {
				$('#main1 .list-el1'+'.'+b).fadeIn('fast');
		});	

});

});

