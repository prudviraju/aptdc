$('.desc').hide();

$(document).ready(function() {

	$('.intro').click(function() {

		if ($(this).parent().hasClass('active-list-el1')) {
			$('.desc').slideUp('slow');
			$('.list-el1').delay( 600 ).removeClass('active-list-el1');
		}	

		else {
			$('.list-el1').delay( 600 ).removeClass('active-list-el1');
			$('.desc').slideUp('slow');
			$(this).next().slideDown('slow');
			$(this).parent().delay( 600 ).toggleClass('active-list-el1');

		}
		
	})
});