Rails.application.routes.draw do
  resolve("ActiveStorage::Variant") {|variant, options| main_app.route_for(:rails_variant, variant, options)}

  resources :users
  resources :order_items
  resources :orders
  resources :images
  resources :plans
  resources :packages
  resources :services
  resources :divisions
  resources :cities do
    get :homePage, :on => :collection
    get :destinations, :on => :collection
    get :location_based_packages, :on => :collection
    get :aboutUs, :on => :collection
    get :contactUs, :on => :collection
    get :add_services_to_session, :on => :collection
    get :service_based_city_pakages, :on => :collection
    get :service_based_city_pakages1, :on => :collection
    get :service_based_packages, :on => :collection
    get :service_based_packages1, :on => :collection
    get :billing_detail, :on => :collection
    get :change_cart_items, :on => :collection
    post :place_order, :on => :collection
    get :pay_zippy_response, :on => :collection
  end


  get "cc_avenue/dataFrom"

  get "cc_avenue/ccavRequestHandler"

  post "cc_avenue/ccavResponseHandler"


  resources :sessions, only: [:new, :create, :destroy]
  match 'login', :to => 'sessions#new', :via => [:get, :post]
  match 'logout', :to => 'sessions#destroy', :via => [:get, :post]



  root 'cities#homePage'
end
